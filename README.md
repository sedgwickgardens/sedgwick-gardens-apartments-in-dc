Sedgwick Gardens puts you in a luxurious Cleveland Park apartment, giving you access to this beautiful neighborhood’s many amenities and pleasures. For pet owners and lovers of the outdoors, Cleveland Park couldn’t be a more perfect place to settle in. Call (855) 850-4719 for more information!

Address: 3726 Connecticut Ave NW, Washington, DC 20008, USA

Phone: 855-850-4719

Website: https://www.daroapartments.com/properties/sedgwick-gardens